package GrockingAlgorithms;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class SelectionSort
{
    /**
     *
     * @param list is {@code List} due to the fact
     * in {@code selectionSort} we find the smallest element,
     * and then we delete it from the list.
     * Deleting from List is faster than from Array
     * @return index of the smallest element
     */
    public static int findSmallest(List<Integer> list)
    {
        int smallest = list.get(0);
        int smallest_index = 0;
        for (int i = 1; i < list.size(); i++) {
            if (list.get(i) < smallest) {
                smallest = list.get(i);
                smallest_index = i;
            }
        }
        return smallest_index;
    }

    public static int[] selectionSort(int[] arr)
    {
        int[] new_array = new int[arr.length];
        // Disassemble int[] into List<Integer>
        List<Integer> list = Arrays.stream(arr).boxed().collect(Collectors.toList());
        for (int i = 0; i < arr.length; i++) {
            int smallest = findSmallest(list);
            new_array[i] = list.get(smallest);
            list.remove(smallest);
        }
        return new_array;
    }

    public static void main(String[] args)
    {
        int num;
        int[] array;
        Scanner in = new Scanner(System.in);
        System.out.print("Input number of elements of array: ");
        num = in.nextInt();
        array = new int[num];
        System.out.print("Input integer elements of array: ");
        for (int i = 0; i < num; i++) {
            array[i] = in.nextInt();
        }
        System.out.println("Original list: " + Arrays.toString(array));
        System.out.println("Sorted array: " + Arrays.toString(selectionSort(array)));
    }
}