package GrockingAlgorithms;

import java.util.Arrays;
import java.util.Scanner;

public class BinarySearch
{
    public static Integer binarySearch(int[] a, int elem)
    {
        int low = 0;
        int high = a.length - 1;
        int mid;
        int guess;
        while (low <= high) {
            mid = (low + high) /2;
            guess = a[mid];
            if (elem == guess) {
                return mid;
            }
            if (guess > elem) {
                high = mid - 1;
            }
            else {
                low = mid + 1;
            }
        }
        return null;
    }

    public static void main(String[] args)
    {
        // Input ...
        int num;
        int[] arr;
        int elem;
        Integer res;
        Scanner in = new Scanner(System.in);
        System.out.print("Enter number of elements of array: ");
        num = Integer.parseInt(in.nextLine());
        arr = new int[num];
        System.out.print("Enter integer elements of array: ");
        for (int i = 0; i < num; i++) {
            arr[i] = in.nextInt();
        }
        // consumes the \n character
        if (in.hasNextLine()) {
            in.nextLine();
        }
        // Sorting ...
        for (int i = 1; i < num; i++) {
            int j = i;
            while (j > 0 && arr[j-1] > arr[j]) {
                int tmp = arr[j - 1];
                arr[j - 1] = arr[j];
                arr[j] = tmp;
                j--;
            }
        }
        // Printing sorted array
        System.out.println("Sorted array:\n" + Arrays.toString(arr));
        // Calling BinarySearch
        System.out.print("Input element to find: ");
        elem = in.nextInt();
        res = binarySearch(arr, elem);
        if (res == null) {
            System.out.println("Element not found!");
            return;
        }
        System.out.println("Element's position is " + res);
    }
}