package GrockingAlgorithms.graph;

public class App
{
    public static void main(String[] args)
    {
        Graph graph = new Graph();

        graph.addNeighbours(
            true,"Me","Alice", "Bob", "Claire");
        graph.addNeighbours(
            true, "Alice", "Peggy");
        graph.addNeighbours(
            true,"Bob", "Peggy", "Anuj");
        graph.addNeighbours(
            true,"Claire", "Thom", "Jonny");

//        graph.addVertex("Me");
//        graph.addVertex("Alice");
//        graph.addVertex("Bob");
//        graph.addVertex("Claire");
//        graph.addVertex("Peggy");
//        graph.addVertex("Anuj");
//        graph.addVertex("Tom");
//        graph.addVertex("Jonny");

//        graph.addEdge(0,1,true);
//        graph.addEdge(0,2,true);
//        graph.addEdge(0,3,true);
//        graph.addEdge(1,4,true);
//        graph.addEdge(2,4,true);
//        graph.addEdge(2,5,true);
//        graph.addEdge(3,6,true);
//        graph.addEdge(3,7,true);
        System.out.println("Depth pass in graph:");
        graph.passInDepth(0);
        System.out.println();
        System.out.println("Breadth pass in graph:");
        graph.passInBreadth(0);
    }
}