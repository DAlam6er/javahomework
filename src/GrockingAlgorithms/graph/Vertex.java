package GrockingAlgorithms.graph;

public class Vertex
{
    private String name;
    private boolean isVisited;

    public Vertex(String name)
    {
        this.name = name;
        isVisited = false;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        if (name != null && !name.isBlank())
        {
            this.name = name;
        }
    }

    public boolean getisVisited()
    {
        return isVisited;
    }

    public void setisVisited(boolean flag)
    {
        isVisited = flag;
    }

    @Override
    public String toString()
    {
        return getName();
    }
}