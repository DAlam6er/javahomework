package GrockingAlgorithms.graph;

import java.util.Arrays;

public class Graph
{
    private final int maxVertex = 10;
    private int[][] adjMatrix; // матрица смежности (Adjacency matrix)
    private int curVertex; // current vertex counter
    Vertex[] vertexList;
    private MyStack stack = new MyStack();
    private MyQueue queue = new MyQueue();

    public Graph()
    {
        vertexList = new Vertex[maxVertex];
        adjMatrix = new int[maxVertex][maxVertex];
        curVertex = 0;
    }

    /**
     * Add vertex to graph
     * @param name name of the vertex
     */
    public void addVertex(String name)
    {
        if (!toString().contains(name)) {
            vertexList[curVertex++] = new Vertex(name);
        }
    }

    /**
     * Adding an edge between two vertices of the graph
     * @param start index of the start vertex
     * @param end index of the end vertex
     * @param directed - directed or undirected graph
     */
    public void addEdge(int start, int end, boolean directed)
    {
        adjMatrix[start][end] = 1;
        if (!directed) {
            adjMatrix[end][start] = 1;
        }
    }

    /**
     * Add multiple neighbours for selected vertex
     * @param directed sign of directed graph
     * @param name name of the start vertex
     * @param neighbor name of the neighbor to be added
     */
    public void addNeighbours(
        boolean directed, String name, String ... neighbor
    )
    {
        int start = indexOf(name);

        if (-1 == start) {
            start = curVertex;
            addVertex(name);
        }

        for (String el : neighbor) {
            addVertex(el);
            addEdge(start, indexOf(el), directed);
        }
    }

    /**
     * Return index of the vertex with the specified name (or -1 if not found)
     * @param name name of the vertex
     * @return index of the vertex with the specified name
     */
    public int indexOf(String name) {
        for (int i = 0; i < curVertex; i++) {
            if(name.equals(vertexList[i].getName())) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Checking vertex of the graph for being visited
     * @param v index of checking vertex
     * @return index of vertex not visited yet
     */
    public int check(int v)
    {
        for (int i = 0; i < curVertex; i++) {
            // if there's a path to vertex, and it is not visited yet
            if (adjMatrix[v][i] == 1 && !vertexList[i].getisVisited()) {
                return i;
            }
        }
        return -1; // if no unvisited vertex is found
    }

    /**
     * Depth pass in graph
     * @param index start index of depth-first searching
     */
    public void passInDepth(int index)
    {
        System.out.println(vertexList[index].getName());
        vertexList[index].setisVisited(true);
        stack.push(index);

        while(!stack.isEmpty()) {
            int neighbor = check(stack.peek());

            if (-1 == neighbor) {
                neighbor = stack.pop();
            } else {
                System.out.println(vertexList[neighbor].getName());
                vertexList[neighbor].setisVisited(true);
                stack.push(neighbor);
            }
        }

        for (int i = 0; i < curVertex; i++) {
            vertexList[i].setisVisited(false);
        }
    }

    /**
     * Breadth pass in graph
     * @param index start index of breadth-first searching
     */
    public void passInBreadth(int index)
    {
        System.out.println(vertexList[index].getName());
        vertexList[index].setisVisited(true);
        queue.insert(index);

        int vertex;

        while(!queue.isEmpty()) {
            int temp = queue.remove();
            while ((vertex = check(temp)) != -1) {
                System.out.println(vertexList[vertex].getName());
                vertexList[vertex].setisVisited(true);
                queue.insert(vertex);
            }
        }

        for (int i = 0; i < curVertex; i++) {
            vertexList[i].setisVisited(false);
        }
    }

    @Override
    public String toString()
    {
        return Arrays.toString(vertexList);
    }
}