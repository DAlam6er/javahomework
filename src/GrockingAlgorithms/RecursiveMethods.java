package GrockingAlgorithms;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class RecursiveMethods {
    public static int sumRecursive(List<Integer> list)
    {
        if (0 == list.size()) {
            return 0;
        }
        return list.get(0) + sumRecursive(list.subList(1,list.size()));
    }

    public static int numberRecursive(List<Integer> list)
    {
        if (0 == list.size()) {
            return 0;
        }
        return 1 + numberRecursive(list.subList(1,list.size()));
    }

    public static int maxRecursive(List<Integer> list)
    {
        if (2 == list.size()) {
            return list.get(1) > list.get(0) ?
                list.get(1) : list.get(0);
        }
        int tmp = maxRecursive(list.subList(1, list.size()));
        return list.get(0) > tmp ? list.get(0) : tmp;
    }

    public static void main(String[] args)
    {
        List<Integer> list = new ArrayList<>();
        Scanner in = new Scanner(System.in);
        System.out.println("Input elements of array to summarise: ");
        while (in.hasNextInt()) {
            list.add(in.nextInt());
        }
        System.out.println("Array is:\n" + list);
        System.out.println("Number of elements is " + numberRecursive(list));
        System.out.println("Sum of the elements is " + sumRecursive(list));
        System.out.println("Max element is " + maxRecursive(list));

    }
}